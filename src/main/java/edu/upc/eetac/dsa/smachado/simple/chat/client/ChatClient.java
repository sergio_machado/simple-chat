package edu.upc.eetac.dsa.smachado.simple.chat.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class ChatClient implements Runnable {
	private BufferedReader reader = null;
	private PrintWriter writer = null;
	private String name;
	private Thread thread;

	public ChatClient(String name) {
		this.name = name;
	}

	public void startClient() {
		thread = new Thread(this, "Client " + name);
		thread.start();
	}

	public void stopClient() {

	}

	public boolean sendJoin() throws IOException {
		writer.println("/join " + name);
		writer.flush();

		String response = reader.readLine();
		return (response.equals("/join_ok"));
	}

	public void sendMessage(String message) {
		writer.println("/message " + name + " " + message);
		writer.flush();
	}

	public void readFromServer() throws IOException {
		String line = reader.readLine();
		if (line.startsWith("/message")) {
			line = line.substring(8).trim();
			String name = line.substring(0, line.indexOf(' '));
			String message = line.substring(line.indexOf(' ') + 1);
			System.out.println(name + "> " + message);
		}
		if (line.startsWith("/event")) {
			line = line.substring(6).trim();
			System.out.println(line);
		}
	}

	public void run() {
		try {
			Socket socket = new Socket(InetAddress.getByName("localhost"), 3333);
			reader = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			writer = new PrintWriter(socket.getOutputStream());

			if (!sendJoin())
				return;

			System.out.println("Connected to the room");
			while (true)
				readFromServer();

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String args[]) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				System.in));
		System.out.println("Enter your name: ");
		String name = reader.readLine();

		ChatClient client = new ChatClient(name);
		client.startClient();

		while (true) {
			String msg = reader.readLine();
			client.sendMessage(msg);
		}

	}
}
