package edu.upc.eetac.dsa.smachado.simple.chat.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ChatServer implements Runnable {
	class ChatServerThread extends Thread {
		private Socket socket = null;
		private BufferedReader reader = null;
		private PrintWriter writer = null;
		private String clientName;
		private boolean connected;

		public ChatServerThread(Socket socket) {
			this.socket = socket;
		}

		public void run() {
			try {
				reader = new BufferedReader(new InputStreamReader(
						socket.getInputStream()));
				writer = new PrintWriter(socket.getOutputStream());

				String line = reader.readLine();
				if (!line.toLowerCase().startsWith("/join ")) {
					sendToClient("/error", "system", "You have to join first!");
					socket.close();
					return;
				}

				clientName = line.substring(6).trim();
				sendToClient("/join_ok");

				broadcast("/event " + clientName + " has joined the room.");
				addClientThread(this);

				connected = true;
				while (connected) {
					readFromClient();
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public void readFromClient() throws IOException {
			String line = reader.readLine();
			if (line.startsWith("/message"))
				broadcast(line);
			if (line.startsWith("/leave")) {
				removeClientThread(this);
				socket.close();
				connected = false;
				broadcast("/event " + clientName + " has left the room.");
			}

		}

		public void sendToClient(String command, String from, String message) {
			sendToClient(command + " " + from + " " + message);
		}

		public void sendToClient(String line) {
			writer.println(line);
			writer.flush();
		}
	}

	private List<ChatServerThread> clientThreads = null;
	private Thread serverThread = null;

	public ChatServer() {
		clientThreads = new ArrayList<ChatServer.ChatServerThread>();
	}

	public void run() {
		System.out.println("Server started...");

		ServerSocket ssocket;
		try {
			ssocket = new ServerSocket(3333);
			while (Thread.currentThread() == serverThread) {
				Socket s = ssocket.accept();

				ChatServerThread thread = new ChatServerThread(s);
				thread.start();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private synchronized void broadcast(String line) {
		for (ChatServerThread client : clientThreads)
			client.sendToClient(line);
	}

	private synchronized void removeClientThread(ChatServerThread thread) {
		clientThreads.remove(thread);
	}

	private synchronized void addClientThread(ChatServerThread thread) {
		clientThreads.add(thread);
	}

	public void startServer() {
		serverThread = new Thread(this, "Chat Server");
		serverThread.start();
	}

	public void stopServer() {
		serverThread = null;
	}

	public static void main(String args[]) throws IOException {
		ChatServer chatServer = new ChatServer();
		chatServer.startServer();
	}
}
